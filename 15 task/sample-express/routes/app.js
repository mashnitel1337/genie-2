const express = require('express');
const router = express.Router();
const faker = require('faker');


router.get('/kurss', (req, res) => {
   var kurss = [ 
       { "name": "Завершенные проекты", "id": "завершенные проекты"}, 
         { "name": "Текущие проекты", "id": "текущие проекты" },
         { "name": "Запланированные проекты", "id": "запланированные проекты" }
      ];
    res.render('kurss', 
  			{ "kurss": kurss}
  	);
});

router.get('/group', (req, res) => {
   var group = [ 
       { "name": "50000$-150000$", "id": "50000$-150000$"}, 
         { "name": "150000$-500000$", "id": "150000$-500000$" },
         { "name": "500000$+", "id": "500000$+" }
      ];
    res.render('group', 
        { "group": group}
    );
});
 

router.get('/students', (req, res) => {
    res.render('students', 
        { "students": [ 
           {"price": faker.finance.amount(), "name": faker.company.companyName(), "id": "Иван", "url":"https://lh3.googleusercontent.com/proxy/H-V2dLWg3rN4e3fjEpvkHskkt5jHVaaBMKSSlX8Jf1cE01oBjcJw6TVe3z8wfPzoZ-Ke5EStZsVVinPX-bUfb2GQAGKMYyMq-i13UWMQQLh3GVKH2CvWNXvr"}, 
           {"price": faker.finance.amount(), "name": faker.company.companyName(), "id": "Иван", "url":"https://img.webmd.com/dtmcms/live/webmd/consumer_assets/site_images/article_thumbnails/slideshows/diseases_still_around_slideshow/650x350_diseases_still_around_slideshow.jpg"},
           {"price": faker.finance.amount(),"name": faker.company.companyName(), "id": "Иван", "url":"https://img.jakpost.net/c/2020/02/20/2020_02_20_87052_1582184356._large.jpg"},
          ]
        }
    );
});

router.get('/active', (req, res) => {
    res.render('active', 
        { "active": [ 
           { "price": faker.finance.amount(), "name": faker.company.companyName(), "url":"https://facknet.ru/wp-content/uploads/2009/09/sverkhchelovek.jpg","description": "Активно участвует в жизни универа"}, 
           { "price": faker.finance.amount(), "name": faker.company.companyName(), "url":"https://5dreal.com/wp-content/uploads/2017/01/16406745_1524819894217614_6286206533653194984_n.jpg","description": "Активно участвует в жизни универа"},
          ]
        }
    );
});

module.exports = router;
